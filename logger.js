"use strict";
/**
 * Copyright 2021 Agilesoft (http://www.agilesoft.cl)
 *
 * Logger module.
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createInstance = exports.Logger = void 0;
const winston_1 = __importDefault(require("winston"));
class Logger {
    constructor(originName) {
        const metadataFormat = winston_1.default.format((info) => {
            if (originName) {
                info.logger = originName;
            }
            return info;
        });
        this.logger = winston_1.default.createLogger({
            level: level(),
            levels,
            // Chose the aspect of your log customizing the log format.
            format: winston_1.default.format.combine(winston_1.default.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss:ms" }), metadataFormat(), winston_1.default.format.json()),
            transports,
        });
    }
    error(message, ...meta) {
        this.logger.error(message, meta);
    }
    info(message, ...meta) {
        this.logger.info(message, meta);
    }
    debug(message, ...meta) {
        this.logger.debug(message, meta);
    }
    warn(message, ...meta) {
        this.logger.warn(message, meta);
    }
}
exports.Logger = Logger;
// Define your severity levels.
// With them, You can create log files,
// see or hide levels based on the running ENV.
const levels = {
    error: 0,
    warn: 1,
    info: 2,
    http: 3,
    debug: 4,
};
// This method set the current severity based on
// the current NODE_ENV: show all the log levels
// if the server was run in development mode; otherwise,
// if it was run in production, show only warn and error messages.
const level = () => {
    const env = process.env.NODE_ENV || "development";
    const isDevelopment = env === "development";
    return isDevelopment ? "debug" : "info";
};
// Define different colors for each level.
// Colors make the log message more visible,
// adding the ability to focus or ignore messages.
const colors = {
    error: "red",
    warn: "yellow",
    info: "green",
    http: "magenta",
    debug: "white",
};
// Tell winston that you want to link the colors
// defined above to the severity levels.
winston_1.default.addColors(colors);
// Define which transports the logger must use to print out messages.
// In this example, we are using three different transports
const transports = [
    // Allow the use the console to print the messages
    new winston_1.default.transports.Console(),
];
const DefaultLogger = winston_1.default.createLogger({
    level: level(),
    levels,
    // Chose the aspect of your log customizing the log format.
    format: winston_1.default.format.combine(winston_1.default.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss:ms" }), winston_1.default.format.json()),
    transports,
});
const createInstance = (originName) => {
    return new Logger(originName);
};
exports.createInstance = createInstance;
exports.default = DefaultLogger;
//# sourceMappingURL=logger.js.map