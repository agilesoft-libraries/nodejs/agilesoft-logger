/**
 * Copyright 2021 Agilesoft (http://www.agilesoft.cl)
 *
 * Logger module.
 */
import winston from "winston";
export declare class Logger {
    private logger;
    constructor(originName?: string);
    error(message: string, ...meta: any[]): void;
    info(message: string, ...meta: any[]): void;
    debug(message: string, ...meta: any[]): void;
    warn(message: string, ...meta: any[]): void;
}
declare const DefaultLogger: winston.Logger;
declare const createInstance: (originName?: string | undefined) => Logger;
export default DefaultLogger;
export { createInstance };
