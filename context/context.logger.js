"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContextLogger = void 0;
const logger_1 = require("../logger");
class ContextLogger extends logger_1.Logger {
    constructor(contextData) {
        super(ContextLogger.name);
        this.contextData = contextData;
    }
    getContext() {
        return Object.assign({}, this.contextData);
    }
    debug(message, ...meta) {
        super.debug(message, this.createExtraData(meta));
    }
    info(message, ...meta) {
        super.info(message, this.createExtraData(meta));
    }
    error(message, ...meta) {
        super.error(message, this.createExtraData(meta));
    }
    warn(message, ...meta) {
        super.warn(message, this.createExtraData(meta));
    }
    createExtraData(...meta) {
        const logExtra = {
            contextData: this.contextData,
        };
        if (meta) {
            logExtra.extra = meta;
        }
        return logExtra;
    }
}
exports.ContextLogger = ContextLogger;
//# sourceMappingURL=context.logger.js.map