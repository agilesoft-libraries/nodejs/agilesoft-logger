import { Logger } from '../logger';
import { ApiLoggerContextData } from './loggerdata.interface';
export interface ContextData {
    data: ApiLoggerContextData;
}
export declare class ContextLogger extends Logger {
    private contextData;
    constructor(contextData: ContextData);
    getContext(): ContextData;
    debug(message: string, ...meta: any[]): void;
    info(message: string, ...meta: any[]): void;
    error(message: string, ...meta: any[]): void;
    warn(message: string, ...meta: any[]): void;
    private createExtraData;
}
