export interface ApiLoggerContextData {
    transaction: {
        trxId: string;
        timestamp: Date;
        context: string;
        module: string;
        service: string;
        action: string;
    };
    client: {
        sessionID: string;
        userID: string;
        secondaryUserID: string;
        executiveID: string;
    };
}
