/**
 * Copyright 2021 Agilesoft (http://www.agilesoft.cl)
 *
 * Http interceptor logger module.
 */
/// <reference types="node" />
declare const morganMiddleware: (req: import("http").IncomingMessage, res: import("http").ServerResponse, callback: (err?: Error | undefined) => void) => void;
export default morganMiddleware;
